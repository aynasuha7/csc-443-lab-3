<?php declare(strict_types=1); // strict requirement ?>
<!DOCTYPE html>
<html>
<body>

<?php
function sum(int $x, int $y) {
$sum = $x + $y;
return $sum;
}

$num1 = 11;
$num2 = 15;
echo "num1 + num2 = " . sum($num1,$num2);
    
 ?>
</body>
</html>